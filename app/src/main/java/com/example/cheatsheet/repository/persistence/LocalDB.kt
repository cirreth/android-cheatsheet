package com.example.cheatsheet.repository.persistence

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.cheatsheet.repository.persistence.dao.UserDao
import com.example.cheatsheet.repository.persistence.models.User


@Database(entities = [User::class], version=4)
abstract class LocalDB: RoomDatabase() {

    abstract fun userDao(): UserDao

}

