package com.example.cheatsheet.repository.persistence.dao

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.cheatsheet.repository.persistence.models.User

@Dao
interface UserDao {

    @Query("SELECT * FROM user")
    fun getAll(): LiveData<List<User>>

    @Query("SELECT * FROM user")
    fun getAllSync(): List<User>

    @Query("SELECT * FROM user WHERE uid = :uid")
    fun getUser(uid: Long): LiveData<User>

    @Query("SELECT * FROM user WHERE name = :name")
    fun getUsersByNameSync(name: String): List<User>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    fun insertAll(vararg users: User): LongArray

    @Update
    fun updateAll(vararg users: User)

    @Delete
    fun delete(user: User)

}
