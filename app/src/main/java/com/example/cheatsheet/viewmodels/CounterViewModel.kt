package com.example.cheatsheet.viewmodels

import androidx.annotation.MainThread
import androidx.lifecycle.*
import com.example.cheatsheet.repository.persistence.LocalDB
import com.example.cheatsheet.repository.persistence.models.User
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Maybe
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.schedulers.Schedulers

class CounterViewModel(val db: LocalDB): ViewModel() {

    val userId = MutableLiveData<Long?>()
    var user: LiveData<User> = Transformations.switchMap(userId) { uid -> if (uid != null) db.userDao().getUser(uid) else MutableLiveData(null) }
    var loginError = MutableLiveData("")

    fun setUser(id: Long) {
        userId.value = id
    }

    @MainThread
    fun login(username: String) {
        Maybe.create<User> {
            it.onSuccess(db.userDao().getUsersByNameSync(username).first())
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(
            {
                userId.value = it.uid
            },
            { err ->
                if (err is NoSuchElementException) {
                    loginError.postValue("User has not been found")
                } else {
                    loginError.postValue(err.toString())
                }
            }
        )
    }

    @MainThread
    fun logout() {
        userId.value = null
    }

    fun increment() {
        Completable
        .create {
            if (user.value != null) {
                user.value!!.counter = user.value!!.counter + 1
                this@CounterViewModel.db.userDao().updateAll(user.value!!)
            }
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe {  }
    }

    fun createUser(user: User) {
        Single
        .create<Long> {
            it.onSuccess(db.userDao().insertAll(user)[0])
        }
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe { userId -> setUser(userId) }
    }

    class Factory(val db: LocalDB): ViewModelProvider.NewInstanceFactory() {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = CounterViewModel(db) as T
    }

}
