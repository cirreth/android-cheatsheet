package com.example.cheatsheet.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.cheatsheet.R
import com.example.cheatsheet.databinding.FragmentCounterBinding
import com.example.cheatsheet.utils.kodeinActivityViewModel
import com.example.cheatsheet.viewmodels.CounterViewModel
import org.kodein.di.DIAware
import org.kodein.di.android.x.closestDI

class CounterFragment: Fragment(R.layout.fragment_counter), DIAware {

    override val di by closestDI()
    private lateinit var binding: FragmentCounterBinding
    private val counterViewModel: CounterViewModel by kodeinActivityViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding =  FragmentCounterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        binding.btnLogout.setOnClickListener(this::logout)
        binding.btnIncrement.setOnClickListener { counterViewModel.increment() }
    }

    private fun logout(view: View) {
        counterViewModel.logout()
        view.findNavController().navigate(R.id.action_counterFragment_to_loginFragment)
    }


}
