package com.example.cheatsheet.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.cheatsheet.R
import com.example.cheatsheet.databinding.FragmentLoginBinding
import com.example.cheatsheet.repository.persistence.models.User
import com.example.cheatsheet.utils.kodeinActivityViewModel
import com.example.cheatsheet.viewmodels.CounterViewModel
import org.kodein.di.DIAware
import org.kodein.di.android.x.closestDI

class LoginFragment: Fragment(R.layout.fragment_login), DIAware {

    override val di by closestDI()
    private lateinit var binding: FragmentLoginBinding
    private val counterViewModel: CounterViewModel by kodeinActivityViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding =  FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.tvError.visibility = View.GONE

        binding.btnCreateUser.setOnClickListener(this::navToCreateUser)
        binding.btnLogin.setOnClickListener { onLoginBtnClick() }

    }

    override fun onStart() {
        super.onStart()

        counterViewModel.loginError.observe(this.viewLifecycleOwner, this::setErrorMessage)

        counterViewModel.user.observe(this.viewLifecycleOwner, { user: User? ->
            user?.let{navToCounterView(this.requireView())}
        })

    }

    private fun setErrorMessage(message: String?) {
        if (message.isNullOrEmpty()) {
            binding.tvError.visibility = View.GONE
        } else {
            binding.tvError.visibility = View.VISIBLE
            binding.tvError.text = message
        }
    }

    private fun navToCreateUser(view: View) {
        view.findNavController().navigate(R.id.action_loginFragment_to_createUserFragment)
    }

    private fun navToCounterView(view: View) {
        view.findNavController().navigate(R.id.action_loginFragment_to_counterFragment)
    }

    private fun onLoginBtnClick() {
        getLoginOrNull(binding.etUsername.text.toString())?.let { username ->
            counterViewModel.login(username)
        }
    }


    private fun getLoginOrNull(login: String?): String? {
        val validationResult = validate(login)
        if (validationResult.isNullOrEmpty()) {
            setErrorMessage(null)
            return login
        } else {
            setErrorMessage(validationResult)
        }
        return null
    }

    private fun validate(login: String?): String? {
        if (login.isNullOrEmpty()) return "Username is empty"
        return null
    }

}



