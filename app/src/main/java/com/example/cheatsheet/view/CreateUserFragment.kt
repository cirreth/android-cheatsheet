package com.example.cheatsheet.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.example.cheatsheet.R
import com.example.cheatsheet.databinding.FragmentCreateUserBinding
import com.example.cheatsheet.repository.persistence.models.User
import com.example.cheatsheet.utils.kodeinActivityViewModel
import com.example.cheatsheet.viewmodels.CounterViewModel
import org.kodein.di.DI
import org.kodein.di.DIAware
import org.kodein.di.android.x.closestDI

class CreateUserFragment: Fragment(R.layout.fragment_create_user), DIAware {

    override val di by closestDI()
    private lateinit var binding: FragmentCreateUserBinding
    private val counterViewModel: CounterViewModel by kodeinActivityViewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentCreateUserBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnCancel.setOnClickListener(this::navToLoginFragment)
        binding.btnCreateUser.setOnClickListener{ createUser(view, binding.etUsername.text.toString()) }
    }

    private fun createUser(view: View, username: String) {
        counterViewModel.createUser(User(name=username, uid = 0, counter = 0))
        view.findNavController().popBackStack()
    }

    private fun navToLoginFragment(view: View) {
        view.findNavController().popBackStack()
    }

}
