package com.example.cheatsheet

import android.os.Bundle
import android.text.InputType
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.example.cheatsheet.databinding.ActivityMainBinding
import com.example.cheatsheet.repository.persistence.models.User
import com.example.cheatsheet.utils.kodeinViewModel
import com.example.cheatsheet.viewmodels.CounterViewModel
import org.kodein.di.DIAware
import org.kodein.di.android.closestDI

class MainActivity : AppCompatActivity(), DIAware {

    // view elements binding
    private lateinit var binding: ActivityMainBinding
    override val di by closestDI()
    private lateinit var navController: NavController
    private val counterViewModel: CounterViewModel by kodeinViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root


        setContentView(view)

        // init navigation
        navController = findNavController(R.id.navHostFragment)

        counterViewModel.user.observe(this, {
            setUserVisibility(it)
            setUserText(it)
        })

    }

    private fun setUserVisibility(user: User?) {
        if (user == null) {
            binding.label.visibility = View.GONE
            binding.counterTextView.visibility = View.GONE
        } else {
            binding.label.visibility = View.VISIBLE
            binding.counterTextView.visibility = View.VISIBLE
        }
    }

    private fun setUserText(it: User?) {
        binding.counterTextView.text = it?.toString() ?: "not set"
    }

    private fun showInputTexDialog() {
        val input = EditText(this)
        input.inputType = InputType.TYPE_CLASS_TEXT
        AlertDialog.Builder(this)
            .setTitle("Имя пользователя")
            .setView(input as View)
            .setPositiveButton("OK") { _, _ ->
                val newUser = User(0, input.text.toString(), 0)
                counterViewModel.createUser(newUser)
            }
            .setNegativeButton("Cancel") { _, _ ->  }
            .create().let {
                it.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE)
                it.show()
            }
    }

}