package com.example.cheatsheet

import android.app.Application
import androidx.lifecycle.ViewModelProvider
import androidx.room.Room
import com.example.cheatsheet.repository.persistence.LocalDB
import com.example.cheatsheet.utils.bindViewModel
import com.example.cheatsheet.viewmodels.CounterViewModel
import org.kodein.di.*
import java.util.concurrent.Executors


class CheatsheetApplication: Application(), DIAware {

    override val di by DI.lazy {

        bind<LocalDB>() with eagerSingleton {
            Room.databaseBuilder(
                this@CheatsheetApplication,  LocalDB::class.java, "cheatsheet-db")
                .fallbackToDestructiveMigration()
                .setQueryCallback({ sqlQuery, bindArgs ->
                    println("SQL Query: $sqlQuery SQL Args: $bindArgs")
                }, Executors.newSingleThreadExecutor())
                .build()
        }

        bind<ViewModelProvider.Factory>() with singleton { CounterViewModel.Factory(instance()) }

        bindViewModel<CounterViewModel>() with provider { CounterViewModel(instance()) }


    }


}