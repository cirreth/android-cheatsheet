package com.example.cheatsheet

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.example.cheatsheet.repository.persistence.LocalDB
import com.example.cheatsheet.repository.persistence.dao.UserDao
import com.example.cheatsheet.repository.persistence.models.User
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.util.concurrent.Executors


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {

    private lateinit var db: LocalDB
    private lateinit var userDao: UserDao

    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("com.example.cheatsheet", appContext.packageName)
    }

    @Test
    @Throws(Exception::class)
    fun writeReadDeleteUser() {
        userDao.getAllSync().forEach { userDao.delete(it) }
        val user = User(name = "Kirill", counter = 0)
        userDao.insertAll(user)
        val extractedUser = userDao.getAllSync().first()
        assertEquals(user.name, extractedUser.name)
        assertEquals(user.counter, extractedUser.counter)
        userDao.delete(user)
    }

    @Before
    fun testRoomDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.databaseBuilder(context, LocalDB::class.java, "cheatsheet-db-test")
                .allowMainThreadQueries()
                .fallbackToDestructiveMigration()
                .setQueryCallback({ sqlQuery, bindArgs ->
                    println("SQL Query: $sqlQuery SQL Args: $bindArgs")
                }, Executors.newSingleThreadExecutor())
                .build()
        userDao = db.userDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

}
